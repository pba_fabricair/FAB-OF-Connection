﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Common;

namespace CMD_FAB_SSH
{
    public class ServerCommands
    {
		public static string host = "srvazfoam1.northeurope.cloudapp.azure.com";
		public static string username = "localadmin";
		public static string password = "Foxcutter1!1";
		public static string[] CFDcommands = {"cartesian2DMesh", "checkMesh", "buoyantBoussinesqSimpleFoam"};
		static string[] AppCommandList = { "Exit application", "Check connection", "Get OpenFOAM cases list", "Start OpenFOAM on selected case", "Execute shell", "Upload new OpenFOAM case", "Delete OpenFOAM case"};
		
		static void Main(string[] args)
        {
		
			var connectionInfo = new PasswordConnectionInfo(host, username, password);
			bool _runstate = true;
			string _caseName;
			string _commandName;
			string _userName = Environment.UserName;
			string _localPathToSave = $@"C:\\Users\{_userName}\";			
			ConsoleSpiner spin = new ConsoleSpiner();
			while (_runstate == true)
			{
				Console.WriteLine("********************");
				int n = 0;
				foreach (string text in AppCommandList)
				{
					Console.WriteLine(n + "-" + text);
					n++;
				}
				Console.WriteLine("Enter command number:");
				int commandNumber = Convert.ToInt32(Console.ReadLine());
				switch (commandNumber)
				{
					case (1):
						TestConnection();
						_runstate = true;
						break;

					case (2):
						GetCases();								
						_runstate = true;
						break;

					case (3):
						using (SshClient client = new SshClient(connectionInfo))
						{
							client.Connect();
							ShellStream shell = client.CreateShellStream("Shell", 0, 0, 0, 0, 1024);
							var output = new StringBuilder();
							while (true)
							{
								output.Append(shell.Read());
								if (output.ToString().Trim().EndsWith("$"))
									break;
							}
							shell.Flush();
							string outputString = output.ToString();
							if (outputString.Contains("Welcome"))
							{
								Console.WriteLine("Connected");
								shell.WriteLine("./startOF");
								output = new StringBuilder();
								while (true)
								{
									output.Append(shell.Read());
									if (output.ToString().Trim().EndsWith("$"))
										break;
								}
								shell.Flush();
								outputString = output.ToString();
								if (outputString.Contains("bash"))
								{
									Console.WriteLine("OpenFOAM enviroment started");
									Console.WriteLine("Enter case name:");
									_caseName = Console.ReadLine();
									Console.WriteLine("Enter OpenFOAM command or type startCFD for simulation:");
									_commandName = Console.ReadLine();
									shell.WriteLine("cd OpenFOAM/localadmin-v1712/run/" + _caseName);
									output = new StringBuilder();
									while (true)
									{
										output.Append(shell.Read());
										if (output.ToString().Trim().EndsWith("$"))
										{
											break;
										}

									}
									
									
									shell.Flush();
									outputString = output.ToString();
									if (outputString.Contains(_caseName))
									{
										if (_commandName == "startCFD")
										{
											foreach (string text in CFDcommands)
											{
												shell.WriteLine(text);
												output = new StringBuilder();
												Console.Write("In progress ");
												while (true)
												{
													spin.Turn();
													output.Append(shell.Read());
													if (output.ToString().Trim().EndsWith("$"))
													{
														break;
													}
													if (output.Length > 1000)
													{
														Console.WriteLine(output);
														output.Clear();
													}
													if (Console.KeyAvailable)
													{
														if (Console.ReadKey(true).Key == ConsoleKey.C)
														{
															shell.WriteLine("\x003");
															output.Append(shell.Read());
															Console.WriteLine(output);
															output.Clear();
															break;

														}
													}

												}
												shell.Flush();
												outputString = output.ToString();
												Console.WriteLine(outputString);
												DownloadImage(outputString, _caseName, _localPathToSave);
											}
										}
										else
										{
											shell.Flush();
											Console.WriteLine("Starting selected OpenFOAM command..");
											shell.WriteLine(_commandName);
											output = new StringBuilder();
											
											while (true)
											{
												spin.Turn();
												output.Append(shell.Read());
												if (output.ToString().Trim().EndsWith("$"))
												{
													break;
												}
												if (output.Length > 100)
												{
													Console.WriteLine(output);
													output.Clear();
												}
												if(Console.KeyAvailable)
												{
													if (Console.ReadKey(true).Key == ConsoleKey.C)
													{
														shell.WriteLine("\x003");
														output.Append(shell.Read());
														Console.WriteLine(output);
														output.Clear();
														break;

													}
												}
												


											}
											outputString = output.ToString();
											DownloadImage(outputString, _caseName, _localPathToSave);
										}
									}
									
								}
								client.Disconnect();
							}
							else
							{
								Console.WriteLine(outputString);
								break;
							}					
						}
						break;

					case (0):
						_runstate = false;
						break;

					case (4):
						ExecuteShell();
						break;

					case (5):
						CopyCase();
						break;

					case (6):
						Console.WriteLine("Posible cases:");
						GetCases();
						Console.WriteLine("Enter case name to delete");
						_caseName = Console.ReadLine();
						DeleteFolder(_caseName);
						GetCases();
						break;

				}
			}			
		}
		static string EnterDirName()
		{
			Console.WriteLine("Add folder path from where upload case (local):");
			string input = Console.ReadLine();
			return input;
		}
		static string EnterCaseName()
		{
			Console.WriteLine("What case name will be on server:");
			string name = Console.ReadLine();
			return name;
		}
		public static string TestConnection()
		{
			string result = "TTT";
			using (SshClient client = new SshClient(host,username,password))
			{
				client.Connect();
				if (client.IsConnected)
				{
					//Console.WriteLine("Connection succeed");
					//Console.WriteLine();
					result = "Conected";
				}
				else
				{
					//Console.WriteLine("Connection failed");
					//Console.WriteLine();
					result = "Not connected";
				}
				client.Disconnect();
			}
			return result;
		}

		public static List<string> GetCasesList()
		{
			var retList = new List<string>();
			using (SshClient client = new SshClient(host, username, password))
			{
				client.Connect();
				if (client.IsConnected)
				{
					var cmd = client.CreateCommand("cd OpenFOAM/localadmin-v1712/run && ls");
					cmd.Execute();
					string result = cmd.Result;
					string[] folders = result.Split('\n');
					foreach (string folder in folders)
					{
						//Console.WriteLine(folder);
						retList.Add(folder);
					}
				}
				client.Disconnect();
			}
			return retList;
		}
		public static void GetCases()
		{
			using (SshClient client = new SshClient(host,username,password))
			{
				client.Connect();
				if (client.IsConnected)
				{
					var cmd = client.CreateCommand("cd OpenFOAM/localadmin-v1712/run && ls");
					cmd.Execute();
					string result = cmd.Result;
					string[] folders = result.Split('\n');
					foreach (string folder in folders)
					{
						Console.WriteLine(folder);
					}
				}
				client.Disconnect();
			}
		}
		static void ExecuteShell()
		{
			using (SshClient client = new SshClient(host,username,password))
			{
				client.Connect();
				ShellStream shell = client.CreateShellStream("shell", 0, 0, 0, 0, 1024);
				var output = new StringBuilder();
				while (true)
				{
					output.Append(shell.Read());					
					if (output.ToString().Trim().EndsWith("$"))
						break;
				}
				shell.Flush();
				string outputString = output.ToString();
				if (outputString.Contains("Welcome"))
				{
					Console.WriteLine("Connected");
				}
				else
				{
					Console.WriteLine(outputString);
				}
				while (true)
				{
					Console.WriteLine("Enter exit ot close APP or CMD");
					string input = Console.ReadLine();
					if (input == "exit")
					{
						client.Disconnect();
						break;
					}
					else
					{
						shell.WriteLine(input);
						output = new StringBuilder();
						while (true)
						{
							output.Append(shell.Read());
							if (output.ToString().Trim().EndsWith("$"))
								break;
						}
						Console.WriteLine(output);
					}
				}
				client.Disconnect();
			}
		}
		static void CopyCase()
		{
			using (var scpClient = new ScpClient(host,username,password))
			{
				scpClient.Connect();
				string DirFromUpload = EnterDirName();
				string DirToUpload = EnterCaseName();
				scpClient.Upload(new DirectoryInfo(@DirFromUpload), "/home/localadmin/OpenFOAM/localadmin-v1712/run/" + DirToUpload);
				Console.WriteLine("Copy end");
			}
		}
		static void DownloadImage(string input, string CaseName, string PathToSave)
		{ 
			if (input.Contains("Generating image:"))
			{
				Console.WriteLine("Simulation Done");
				int startIndex = input.IndexOf("Generating image:");
				int endIndex = input.IndexOf("End");
				string _imgDirectory = input.Substring(startIndex, endIndex - startIndex);
				startIndex = _imgDirectory.IndexOf('"');
				endIndex = _imgDirectory.IndexOf('"', startIndex + 1);
				_imgDirectory = _imgDirectory.Substring(startIndex + 1, endIndex - startIndex - 1);
				Console.WriteLine("Do you want to download result image? (Y/N)");
				string answer = Console.ReadLine();
				if (answer == "Y")
				{
					using (var scpClient = new ScpClient(host,username,password))
					{
						scpClient.RemotePathTransformation = RemotePathTransformation.ShellQuote;
						scpClient.Connect();
						using (Stream localFile = File.Create(PathToSave + CaseName + ".png"))
						{
							scpClient.Download(_imgDirectory, localFile);
						}
						Console.WriteLine("It is saved on {0}", PathToSave);
					}
				}
			}
			else
			{
				Console.WriteLine(input);
			}
		}
		static void DeleteFolder(string FolderName)
		{
			using (SshClient client = new SshClient(host, username, password))
			{
				client.Connect();
				if(client.IsConnected)
				{
					ShellStream shell = client.CreateShellStream("Shell", 0, 0, 0, 0, 1024);
					string cmd = "cd OpenFOAM/localadmin-v1712/run && rm -rf " + FolderName;
					Console.WriteLine(cmd);
					shell.WriteLine(cmd);
					shell.Flush();
					var output = new StringBuilder();
					string outputString;

					while (true)
					{
						output.Append(shell.Read());
						if (output.ToString().Trim().EndsWith("$"))
						{
							break;
						}
						if (output.Length > 100)
						{
							Console.WriteLine(output);
							output.Clear();
						}
					}
					shell.Flush();
					outputString = output.ToString();
					Console.WriteLine(outputString);
				}
				else
				{
					Console.WriteLine("No connection");
				}
				client.Disconnect();
			}
		}
	}

	public class ConsoleSpiner
	{
		int counter;
		public ConsoleSpiner()
		{
			counter = 0;
		}
		public void Turn()
		{
			counter++;
			switch (counter % 4)
			{
				case 0: Console.Write("/"); break;
				case 1: Console.Write("-"); break;
				case 2: Console.Write("\\"); break;
				case 3: Console.Write("|"); break;
			}
			Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
		}
	}
	
}
